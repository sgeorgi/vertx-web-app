package io.sgeorgi

import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import org.slf4j.LoggerFactory

fun main(args: Array<String>) {
  val vertx = Vertx.vertx()
  val server = vertx.createHttpServer()
  val router = Router.router(vertx)
  val logger = LoggerFactory.getLogger("VertxServer")

  router.get("/").handler { routingContext ->
    val response = routingContext.response()
    response.end("Hello World!")
  }

  server
      .requestHandler { router.accept(it) }
      .listen(8080) { handler ->
        if (!handler.succeeded()) logger.error("Failed to listen to port 8080")
      }
}